import React, { Component } from 'react';
import {
  StyleSheet,
  AppRegistry,
  Text,
  View,
  TouchableNativeFeedback
} from 'react-native';
import styles from './styles';

export default class CardView extends Component {
	console.log(styles);
  render() {
    //React.Children.forEach(this.props.children, child => console.log(child));
    return (
      <TouchableNativeFeedback>
        <View style={[this.props.style, styles.card]}>
          {this.props.children}
        </View>
      </TouchableNativeFeedback>
    );
  }
}

// const styles = StyleSheet.create({
//   title: {
//     fontWeight: 'bold',
//     fontSize: 25
//   },
//   card: {
//     margin: 10,
//     padding: 10,
//     backgroundColor: 'green',
//     flexDirection: 'row',
//     borderRadius: 5
//   },
//   image: {
//     height: 100,
//     width: 100
//   }
// });
