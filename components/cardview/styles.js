import {StyleSheet} from 'react-native';

export default const styles = StyleSheet.create({
    title: {
        fontWeight: 'bold',
        fontSize: 25
      },
      card: {
        margin: 10,
        padding: 10,
        backgroundColor: 'green',
        flexDirection: 'row',
        borderRadius: 5
      },
      image: {
        height: 100,
        width: 100
      }
  });