import React, { Component } from 'react';
import { StyleSheet, AppRegistry, Text, View } from 'react-native';

export default class CardTitle extends React.Component {
  constructor(props) {
    super(props);
    this.className = 'CardTitle';
    this.state = {
      value: this.props.children
    };
  }

  render() {
    return (
      <Text style={[this.props.style, styles.title]}>{this.state.value}</Text>
    );
  }
}
const styles = StyleSheet.create({
  title: {
    fontWeight: 'bold',
    fontSize: 30
  },
  card: {
    backgroundColor: 'green',
    height: 100,
    width: 200,
    borderRadius: 5
  },
  image: {
    height: 100,
    width: 100
  }
});
