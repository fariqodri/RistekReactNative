import React, { Component } from 'react';
import {
  StyleSheet,
  AppRegistry,
  Text,
  View,
  TouchableNativeFeedback
} from 'react-native';

export default class CardContent extends React.Component {
  constructor(props) {
    super(props);
    this.className = 'CardContent';
    this.state = {
      value: this.props.children
    };
  }

  render() {
    return <Text style={[this.props.style]}>{this.state.value}</Text>;
  }
}

const styles = StyleSheet.create({
  title: {
    fontWeight: 'bold',
    fontSize: 25
  },
  card: {
    backgroundColor: 'green',
    height: 100,
    width: 200,
    borderRadius: 5
  },
  image: {
    height: 100,
    width: 100
  }
});
