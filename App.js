/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image } from 'react-native';
import CardView from './components/cardview/CardView';
import CardContent from './components/cardview/CardContent';
import CardTitle from './components/cardview/CardTitle';

export default class App extends Component {
  render() {
    return (
      <CardView>
        <View style={{ flex: 1, alignItems: 'flex-start' }}>
          <CardTitle>HOHO</CardTitle>
          <CardContent style={{ flex: 1 }}>HEHE</CardContent>
        </View>
        <Image source={require('./static/img/resume128.png')} />
      </CardView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5
  }
});
